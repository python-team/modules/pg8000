Source: pg8000
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Dominik George <nik@naturalnet.de>,
 Rahul Amaram <amaramrahul@users.sourceforge.net>,
 Alexander Sulfrian <asulfrian@zedat.fu-berlin.de>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 python3-all,
 python3-dateutil,
 python3-hatchling,
 python3-scramp,
 python3-setuptools (>= 68.1),
Standards-Version: 4.6.2
Homepage: https://github.com/tlocke/pg8000
Vcs-Git: https://salsa.debian.org/python-team/packages/pg8000.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pg8000
Rules-Requires-Root: no

Package: python3-pg8000
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Pure-Python PostgreSQL Driver (Python 3)
 pg8000 is a Pure-Python interface to the PostgreSQL database engine.  It is
 one of many PostgreSQL interfaces for the Python programming language. pg8000
 is somewhat distinctive in that it is written entirely in Python and does not
 rely on any external libraries (such as a compiled Python module, or
 PostgreSQL's libpq library). pg8000 supports the standard Python DB-API
 version 2.0.
 .
 pg8000's name comes from the belief that it is probably about the 8000th
 PostgreSQL interface for Python.
 .
 This package contains the Python 3 version.

Package: python-pg8000-doc
Architecture: all
Section: oldlibs
Depends: ${misc:Depends}
Description: transitional package
 This is a transitional package. It can safely be removed.
